#include<stdio.h>

void main()
{
	int a[100][100], b[100][100], c[100][100]={0}, d[100][100]={0};
	int x, y, z, r1, c1, r2, c2;
	printf("Enter no. of rows and columns in Matrix 1: \n");
	scanf("%d%d", &r1, &c1);
	printf("Enter no. of rows and columns in Matrix 2: \n");
	scanf("%d%d", &r2, &c2);
	if(r1!=r2 || c1!=c2)
	{
		printf("This Matrix Addition is not possible");
		return;
	}
	else if(c1!=r2)
	{
		printf("This Matrix Multiplication is not possible");
		return;
	}
	else
	{
		printf("Enter the elements of matrix 1: \n");
		for(x=0; x<r1; x++)
			for(y=0; y<c1; y++)
				scanf("%d", &a[x][y]);
		printf("Enter the elements of matrix 2: \n");
		for(x=0; x<r2; x++)
			for(y=0; y<c2; y++)
				scanf("%d", &b[x][y]);

		for(x=0; x<r1; x++)
			for(y=0; y<c1; y++)
				c[x][y] = a[x][y] + b[x][y];
		printf("Matrix Addition:\n");
		for(x=0; x<r1; x++)
		{
			for(y=0; y<c1; y++)
				printf("%d ", c[x][y]);
			printf("\n");
		}

		for(x=0; x<r1; x++)
			for(y=0; y<c2; y++)
				for(z=0; z<r2; z++)
					d[x][y] += a[x][z]*b[z][y];
		printf("Matrix Multiplication:\n");
		for(x=0; x<r1; x++)
		{
			for(y=0; y<c2; y++)
				printf("%d ", d[x][y]);
			printf("\n");
		}
	}

}
